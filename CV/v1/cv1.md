Josep Casanovas Vidal
============
Email: jcasanovasv@hotmail.com
Tel: 636647166
Web: https://www.linkedin.com/in/josef21296/

I am a 21 years old game developer student currently finishing my degree. I have focused on the game programming side using engines like Unity, Unreal Engine 4 and even own engine in C++.

## SKILLS

  - C++: SDL2 OpenGL 
  - Unity: C# scripting Editor scripting 
  - Unreal Engine 4: Blueprints Materials 
  - Graphics programming: GLSL Processing p5.js Pygame 

## EMPLOYMENT

### *Unreal Blueprint Programmer*, [Cygnus Void](http://cygnusvoid.es/) (2018-02 — Present)


  - Implementation of new game mechanics
  - Advise on assets optimization


## PROJECTS

### *Gameplay programmer*, [Ghost Finders](https://undistinguishedfellows.github.io/GJ_GhostFinders/) (??? — Present)

You are a photographer that must hunt as many ghosts as you can using the different light colors.
A game developed in 48 hours for the 2017 Global Game Jam

### *Gameplay programmer*, [Noisy Thief](https://ldjam.com/events/ludum-dare/40/noisy-thief) (??? — Present)

You are a sneaky thief that is trying to steal the three main gems of the museum. Avoid the guards with your habilities and your intelligence.
A game developed in 72 hours for the LudumDare 40

### *C++ programmer*, [JayEngine](https://github.com/Josef212/JayEngine) (??? — Present)


3D game engine made in a context of learning the basics of Game engine development.

### *Gameplay programmer*, [AR space invaders](https://github.com/UndistinguishedFellows/P3_AR_UnityARGame) (??? — Present)


A simple AR game for learning pourposes.

### *AI programmer*, [Farmia](https://bitbucket.org/undistinguishedfellows/farmiaproject) (??? — Present)

Farming simulator where you can expand your lands and hire more people to get rich.
A game made with Unity for AI learning pourposes.

### *C++ Programmer*, [Windows pinball tribute](https://github.com/UndistinguishedFellows/WindowsPinballTribute) (??? — Present)


A simple classic windows pinball tribute for Box2D learning pourposes.

### *C++ Programmer*, [Racing bullet 3D](https://github.com/UndistinguishedFellows/RacingBullet3D) (??? — Present)


A simple 3D car game for Bullet 3D learning pourposes.

### *C++ and GLSL Programmer*, [Graphics Engine](https://github.com/UndistinguishedFellows/GraphicsEngine) (??? — Present)

A simple graphics engine for learning pourposes.
A simple graphics engine for learning pourposes.

### *C++ Programmer*, [GUI research](https://gui-research.github.io/GUI_Research/) (??? — Present)

A simple gui research on animations and transitions for learning pourposes.
A simple gui research on animations and transitions for learning pourposes.



## EDUCATION

### [CITM](https://www.citm.upc.edu/) (2004-09 — 2018-06)

Degree on game design and development.


### [Escola Pia Igualada](https://epiaigualada.cat/) (2012-09 — 2014-06)

High school studies before university.












## INTERESTS

- SCUBA DIVING: Sea Scuba diving 
Sportive scuba diving since I was 9 years old.

- GITHUB: GitHub git GitHub Desktop LFS 
Regular GitHub user and tinkerer.


