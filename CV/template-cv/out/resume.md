Your Name
============
Email: your-email@your-website.com
Tel: 1-999-999-9999
Web: http://your-website.com

A brief description of yourself as a candidate that appears in some résumé themes. You may use **Markdown** and/or <strong>HTML</strong> formatting here including [links](https://en.wikipedia.org/wiki/Special:Random) or stick to plain text.

## SKILLS

  - Web Development: LAMP JavaScript HTML 5 Angular.js jQuery Sass LESS 
  - Web : LAMP 
  - Ninjitsu: Tokagure Ryu Shuriken Yogen 
  - Skillset #3: Your keywords here 

## EMPLOYMENT

### *Head Honcho*, [Most Recent Employer](http://employer-website.com) (2013-10 — Present)

A summary of your role in this position. Can include **Markdown** and inline <strong>HTML</strong> formatting.
  - Increased profits on left-handed spin widgets by 35%.
  - Participated in iterative/incremental devolution.
  - Promoted to Head Ninja after 500 successful cage matches.

### *Worker Bee*, [Previous Employer](http://employer-website.com) (2009-07 — 2013-10)

A summary of your previous role. Can include **Markdown** and inline <strong>HTML</strong> formatting.
  - Highlight #1.
  - Highlight #2.
  - Highlight #3.


## PROJECTS

### *Contributor*, [Awesome Project #1](http://project-page.org) (??? — Present)

A description of the project, for audiences that may not be familiar with it. Like the summary section, can include **Markdown** and inline <strong>HTML</strong> formatting.
Show off your project work with a Markdown-and-HTML friendly summary of your role on the project. Software projects, creative projects, business projects can all appear here.
  - Project Highlight #1

  - Project Highlight #2

  - Project Highlight #3


### *Creator*, [Asteroids](http://asteroids-demo.org) (??? — Present)

A 3D Asteroids clone with Unreal 4 and cross-platform C++.
Conceived, designed, implemented, and created 3D Asteroids space shooter with Unreal Engine IV and C++.
  - Project Highlight #1

  - Project Highlight #2

  - Project Highlight #3




## EDUCATION

### [Acme University](http://acmeuniversity.edu) (2004-09 — 2009-06)

Graduate of Acme University, summer of 2009. Go [Coyotes](https://en.wikipedia.org/wiki/Coyote)!




## SAMPLES

### [Work Sample #1](http://project-repo.com) (2015-01)

A technical or creative **work sample**.

### [Creative Sample #2](http://project-repo.com) (2015-01)

Another technical or creative sample. You can use [Markdown](https://daringfireball.net/projects/markdown/) here.


## WRITING

### ***[My Blog](http://coding-snorer.com)*** (Medium.com, 2010-01)
Your blog or online home.

### ***[Something I Wrote Once](http://codeproject.com/something-i-wrote-once)*** (Acme University Press, 2009-01)
List your blogs, articles, essays, novels, and dissertations.


## READING

### [*Other Copenhagens*](http://www.amazon.com/Other-Copenhagens-And-Stories/dp/0984749209), Edmund Jorgensen

### [*r/programming*](https://www.reddit.com/r/programming)

### [*Hacker News*](https://news.ycombinator.com/)


## SERVICE

### *Volunteer Coordinator*, [The Mommies Network](http://themommiesnetwork.org) (2006-06 — Present)

A summary of your volunteer experience at this organization. Can include **Markdown** and inline <strong>HTML</strong> formatting.

### *MOS*, [Air Force](https://service-website.mil) (2004-01 — Present)

A summary of your service experience with this organization. Can include **Markdown** and inline <strong>HTML</strong> formatting.


## RECOGNITION

### Awesomeness Award, HackMyResume (Jan 2016)
Thanks for being a HackMyResume / FluentCV user!

### MVP: SpinWidget Technology, Big Software (Jan 2015)
For work in promotion and popularization of SpinWidget technology.




## REFERENCES

### *John Doe*, Manager @ Somewhere Inc. (professional)
Contact information available on request.
: john.doe@somewhere.com

### *Jane Q. Fullstacker*, Coworker (technical)
Contact information available on request.
: jane.fullstacker@somewhere.com


## INTERESTS

- GITHUB: GitHub git GitHub Desktop (OS X) LFS GitHub API 
Regular GitHub user and tinkerer.

- CHESS: Sicilian King's Gambit Ruy Lopez 
Avid chess player.


